function getBlockedCommenters(){
  console.log("commentFilterer.js, requesting blocked users from background script");
  chrome.runtime.sendMessage({ action : "get_blocked_commenters" });
}  
  
chrome.runtime.onMessage.addListener(
  function(commentersToBlock, sender, sendResponse){
    console.log("commentFilterer.js, got blocked commenters from background script: " + commentersToBlock);
    var comments = document.getElementsByClassName("d-comment");
    var commentsAsArray = Array.prototype.slice.call(comments);    
    for (var i=0; i<commentsAsArray.length; i++){
      var comment = commentsAsArray[i];
      if (commentersToBlock.indexOf(comment.getAttribute("data-comment-author-id")) != -1){
        var element = document.getElementById(comment.id);
        element.parentNode.removeChild(element);
      }
    }
  });

// TODO, find a better hook to fire this.
setTimeout(getBlockedCommenters, 5000);