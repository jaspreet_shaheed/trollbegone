    chrome.storage.sync.get({'blockedCommenters':[]},
      function(obj){
        console.log("popup.js, retrieved blocked commenters: " + obj.blockedCommenters);
        var blockedCommenterTable = document.getElementById("blockedCommenters");
        for (var i=0; i<obj.blockedCommenters.length; i++){
          var commenterID = obj.blockedCommenters[i];
          var row = blockedCommenterTable.insertRow(1);
          var commenterIDCell = row.insertCell(0);
          var link = document.createElement("a");
          link.id = commenterID + 'linkID';
          link.setAttribute('href', "https://profile.theguardian.com/user/id/" + commenterID);  // since we're defining an onclick this link won't be followed
          link.onclick = displayCommenterInto(commenterID);
          var linkText = document.createTextNode(commenterID);
          link.appendChild(linkText);
          commenterIDCell.appendChild(link);
          
          var unblockButtonCell = row.insertCell(1);
          var unblockButton = document.createElement('button');
          unblockButton.id = commenterID + 'buttonID';
          
          unblockButton.innerHTML = "Unblock";
          unblockButton.onclick = unblockCommenter(unblockButton.id, commenterID);
          unblockButtonCell.appendChild(unblockButton);
        }
      }
    );
    
    function unblockCommenter(buttonID, commenterID)
    {
      return function(){
        console.log("popup.js, buttonID: " + buttonID + " commenter: " + commenterID);
        var button = document.getElementById(buttonID);
        button.disabled = true; 
        
        // Send a message to background.js so the user is removed from the store.
        chrome.runtime.sendMessage({ action: "unblock_commenter", commenterID: commenterID});        
      };
    }
    
    function displayCommenterInto(commenterID)
    {
      return function(){
         chrome.tabs.create({url : "https://profile.theguardian.com/user/id/" + commenterID}); 
      };
    }
