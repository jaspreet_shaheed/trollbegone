chrome.runtime.onInstalled.addListener(
  function(){
    chrome.contextMenus.create({
        id: "blockCommenterMenu",
        title: "Block Commenter",
        contexts: ["link"],
        documentUrlPatterns: ["https://www.theguardian.com/*"]
    });    
  });

function checkForValidUrl(tabId, changeInfo, tab) {
  if (tab){
    if (tab.url.match(/theguardian/)) {
      chrome.pageAction.show(tabId);
    }
  }
}

chrome.tabs.onUpdated.addListener(checkForValidUrl);
chrome.tabs.onCreated.addListener(checkForValidUrl);

chrome.contextMenus.onClicked.addListener(function(info, tab) {
  console.log("background js, user attempted to block following URL: " + info.linkUrl);
  if (info.linkUrl.indexOf("https://profile.theguardian.com/user/id/") === 0)
  {
    var commenterID = info.linkUrl.substring(40);
    chrome.storage.sync.get({'blockedCommenters':[]}, 
      function(obj){
        if (obj.blockedCommenters.indexOf(commenterID) != -1)
        {
          // If the commenter was already blocked, this suggests something has gone wrong, try redoing the filter
          console.log("Commenter " + commenterID + " was already blocked. Most likely some problem with starting the filter");
              chrome.tabs.sendMessage(
              tab.id, 
              obj.blockedCommenters);          
        } else {
          obj.blockedCommenters.push(commenterID);
          chrome.storage.sync.set(obj,
          // Send this message to the tab so comments from the user are blocked.
          function(){
            chrome.tabs.sendMessage(
              tab.id,
              [commenterID]
            );
          });               
        }
      });
  } else {
    alert("Does not appear to be a link to a user");
  }
});

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) 
  {
      console.log("background.js, following request received: " + request + " from " + sender);
      switch (request.action) {
        case "get_blocked_commenters":
          chrome.storage.sync.get({'blockedCommenters':[]},
          function(obj){
              chrome.tabs.sendMessage(
              sender.tab.id, 
              obj.blockedCommenters
            );
          });
          break;
      case "unblock_commenter":
          chrome.storage.sync.get({'blockedCommenters':[]},
          function(obj){
            var index = obj.blockedCommenters.indexOf(request.commenterID);
            obj.blockedCommenters.splice(index,1);
            chrome.storage.sync.set(obj, function(){
                sendResponse({action:"done"});
            });
          });
    }
  });
  
  
// For now, not using this
function addLoadEvent(func) {
  if (window.addEventListener)
    window.addEventListener("load", func, false);
  else if (window.attachEvent)
    window.attachEvent("onload", func);
  else { // fallback
    var old = window.onload;
    window.onload = function() {
      if (old) old();
      func();
    };
  }
}  
